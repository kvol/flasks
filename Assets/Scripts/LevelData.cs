using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Add New Level", order = 1)]
public class LevelData : ScriptableObject
{
    [SerializeField] private List<Flasks> _flasksList;

    public List<Flasks> FlasksList => _flasksList;


    [System.Serializable]
    public struct Flasks
    {
        [SerializeField] private List<ColorsList> _waterPartsColorList;

        public List<ColorsList> WaterPartsColorList => _waterPartsColorList;
    }
}