﻿using UnityEngine;

public class Waterfall : Water
{
    private SpriteRenderer _spriteRenderer;

    protected override void Awake()
    {
        base.Awake();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public float Height
    {
        get => _spriteRenderer.size.y;
        set
        {
            Vector2 size = _spriteRenderer.size;
            size.y = value;
            _spriteRenderer.size = size;
        }
    }
}