using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class Flask : MonoBehaviour, IPointerClickHandler, IMaterialChanger
{
    public const byte MAXWater = 4;

    public event Action<Flask> FlaskAnimationEnded;
    public event Action<Flask> FlaskComplete;

    
    private bool _isComplete;
    
    public bool IsComplete => _isComplete;

    [SerializeField] private SpriteRenderer _maskSpriteRenderer;
    private SpriteRenderer _spriteRenderer;

    public Stack<WaterPart> WaterInside { get; private set; }
    
    public Rect FlaskRect
    {
        get
        {
            var size = _spriteRenderer.size;
            return new Rect((Vector2) transform.position + new Vector2(0, -size.y), size);
        }
    }

    public AnimationController AnimationController { get; private set; }
    public Vector3 BasePosition { get; set; }
    public int Number { get; set; }


    private void Awake()
    {
        WaterInside = new Stack<WaterPart>(GetComponentsInChildren<WaterPart>());
        _spriteRenderer = GetComponent<SpriteRenderer>();
        AnimationController = GetComponent<AnimationController>();
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(FlaskRect.center, FlaskRect.size);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ClickManager.Instance.SelectFlask(this);
    }


    public void Pour(int newWaterCount, AnimationController secondFlaskAnimationController)
    {
        for (var i = 0; i < newWaterCount; i++)
        {
            var water = WaterInside.ElementAt(newWaterCount - i - 1);
            SetWaterPosition( water, i,newWaterCount);
            ShrinkWater(water);
            SetWaterMaterial(water);
            AnimationController.AnimationEnded += EndWaterAnimationHandler;
            AnimationController.AddWaterToAnimate(water, secondFlaskAnimationController,
                AnimationController.AnimationType.PourAnimation);
        }
        CheckWaterComposition();
    }

    private void CheckWaterComposition()
    {
        int matchesNumber = 0;
        ColorsList currentCompositionColor = WaterInside.Peek().Color;
        foreach (var waterPart in WaterInside)
        {
            if (waterPart.Color == currentCompositionColor)
            {
                matchesNumber++;
            }
            else
            {
                break;
            }
        }

        if (matchesNumber == MAXWater)
        {
            _isComplete = true;
        }
    }

    public void ShrinkWater(WaterPart water)
    {
        water.transform.localScale = new Vector3(1, 0);
    }

    public void SetWaterMaterial(WaterPart water)
    {
        MaterialChanger.Instance.ChangeMaterialRefValue(water,Number);
    }
    
    public void SetWaterPosition(WaterPart water, int waterPartNumber=0,int newWaterCount=0)
    {
        Transform waterTransform;
        (waterTransform = water.transform).SetParent(transform);
        waterTransform.localPosition = new Vector2(FlaskRect.width / 2,
            -FlaskRect.height + WaterInside.Count - newWaterCount + waterPartNumber);
    }

    public void Empty(int newWaterCount, AnimationController secondFlaskAnimationController)
    {
        EnableAllWaterMeshScripts();
        for (var i = 0; i < newWaterCount; i++)
        {
            var water = WaterInside.Pop();

            AnimationController.AnimationEnded += EndWaterAnimationHandler;
            AnimationController.AddWaterToAnimate(water, secondFlaskAnimationController,
                AnimationController.AnimationType.EmptyAnimation);
        }
       
    }


    private void EndWaterAnimationHandler()
    {
        AnimationController.AnimationEnded -= EndWaterAnimationHandler;
        FlaskAnimationEnded?.Invoke(this);
        if (AnimationController.RunningAnimationType == AnimationController.AnimationType.EmptyAnimation)
        {
            DisableAllWaterMeshEditorScripts();
        }

        if (_isComplete)
        {
            FlaskComplete?.Invoke(this);
        }
        
    }
    

    private void EnableAllWaterMeshScripts()
    {
        foreach (var waterPart in WaterInside)
        {
            waterPart.EnableWaterMeshEditorScript();
        }
    }

    private void DisableAllWaterMeshEditorScripts()
    {
        foreach (var waterPart in WaterInside)
        {
            waterPart.DisableWaterMeshEditorScript();
        }
    }


    public void SetToPoolAllWaterInside()
    {
        foreach (var water in WaterInside)
        {
            ObjectPool.Instance.SetPooledWater(water);
        }
    }

    public Material GetMaterial()
    {
        return _maskSpriteRenderer.material;
    }

    public void SetMaterial(Material material)
    {
        _maskSpriteRenderer.material = material;
    }
}