using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public event Action AnimationEnded;
    public event Action StartPouringAnimation;

    private Queue<WaterPart> _waterToAnimateList;

    [SerializeField] private float _baseTimeOfAnimation;

    private float _animationSpeed;
    private float _animationRotationSpeed = 15;

    private bool _isAnimationRunning;

    private Func<WaterPart, IEnumerator> _waterAnimation;

    private AnimationType _runningAnimationType = AnimationType.Default;

    private AnimationController _secondFlaskAnimationController = null;

    private FlaskSide _flaskSide;

    [SerializeField] private Vector3[] _animationFlaskPath;

    private bool _startAnimation = true;
    [SerializeField] private float _baseRequiredOffset = 0.75f;
    [SerializeField] [Range(0, 1f)] private float _speedReductionMultiplier;
    [SerializeField] private float _timeOfAnimation;
    [SerializeField] private Waterfall _waterfallPrefab;
    private Waterfall _instantiatedWaterfall;

    public enum AnimationType
    {
        Default = 0,
        PourAnimation,
        EmptyAnimation
    }

    private enum FlaskSide
    {
        Left = 1,
        Right = -1
    }

    private Vector3 RotationPoint
    {
        get
        {
            var transform1 = transform;

            if (_flaskSide == FlaskSide.Left)
            {
                return transform1.position;
            }

            float angle = transform1.eulerAngles.z * Mathf.Deg2Rad;
            return transform1.position + new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0);
        }
    }

    public AnimationType RunningAnimationType
    {
        get => _runningAnimationType;
        private set
        {
            if (_runningAnimationType != value && _isAnimationRunning)
                throw new Exception("Trying to change animation type during animation");
            _runningAnimationType = value;
            _waterAnimation += value switch
            {
                AnimationType.PourAnimation => PourAnimation,
                AnimationType.EmptyAnimation => EmptyAnimation,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }

    private void Awake()
    {
        _waterToAnimateList = new Queue<WaterPart>();
    }

    public void AddWaterToAnimate(WaterPart waterToAnimate, AnimationController secondFlaskAnimationController,
        AnimationType animationType)
    {
        _secondFlaskAnimationController ??= secondFlaskAnimationController;

        _waterToAnimateList.Enqueue(waterToAnimate);

        CalculateAnimationSpeed();

        if (_isAnimationRunning)
        {
            return;
        }

        RunningAnimationType = animationType;

        StartAnimation();
    }

    private void OnStartPouringAnimation()
    {
        _secondFlaskAnimationController.StartPouringAnimation -= OnStartPouringAnimation;
        _startAnimation = true;
    }

    private void CalculateAnimationSpeed()
    {
        float timeOfAnimation = _baseTimeOfAnimation *
                                (1 - Mathf.Pow(_speedReductionMultiplier, _waterToAnimateList.Count)) /
                                (1 - _speedReductionMultiplier);
        _timeOfAnimation = timeOfAnimation;
        float newSpeed = _waterToAnimateList.Count / timeOfAnimation;

        DOVirtual.Float(_animationSpeed, newSpeed, (1 - _speedReductionMultiplier) * _timeOfAnimation,
            n => _animationSpeed = n);
    }

    private void StartAnimation()
    {
        if (_runningAnimationType == AnimationType.PourAnimation)
        {
            _secondFlaskAnimationController.StartPouringAnimation += OnStartPouringAnimation;
            _startAnimation = false;
        }

        _isAnimationRunning = true;
        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        yield return new WaitUntil(() => _startAnimation);
        WaterPart waterToAnimate = _waterToAnimateList.Peek();
        int numberInFlask = waterToAnimate.GetNumberRelativeToTopOfFlask();
        Vector3 returnPoint = GetComponent<Flask>().BasePosition;

        if (RunningAnimationType == AnimationType.EmptyAnimation)
        {
            Rect secondFlaskRect = _secondFlaskAnimationController.GetComponent<Flask>().FlaskRect;
            ChooseFlaskSide();

            AimFlaskOnSecondFlask(secondFlaskRect);
            yield return StartCoroutine(FlaskEmptyAnimation(numberInFlask));
            StartWaterfallAnimation();
            StartCoroutine(FlaskEmptyAnimation(numberInFlask + _waterToAnimateList.Count));
        }

        while (_waterToAnimateList.Count > 0)
        {
            waterToAnimate = _waterToAnimateList.Peek();

            yield return StartCoroutine(_waterAnimation(waterToAnimate));
            _waterToAnimateList.Dequeue();
        }


        if (_runningAnimationType == AnimationType.EmptyAnimation)
        {
            EndWaterfallAnimation();
            AimFlaskBack(returnPoint);
        }

        transform.DORotate(Vector3.zero, _baseTimeOfAnimation / 2).OnComplete(() =>
        {
            _secondFlaskAnimationController = null;
            _isAnimationRunning = false;
            AnimationEnded?.Invoke();
        });
    }

    private void ChooseFlaskSide()
    {
        _flaskSide = transform.position.x - _secondFlaskAnimationController.transform.position.x > 0
            ? FlaskSide.Left
            : FlaskSide.Right;
    }

    private void StartWaterfallAnimation()
    {
        _instantiatedWaterfall = Instantiate(_waterfallPrefab, transform);
        Transform waterfallTransform = _instantiatedWaterfall.transform;
        waterfallTransform.localPosition =
            _flaskSide == FlaskSide.Left ? new Vector3(0.11f, 0, 0) : new Vector3(1 - 0.11f, 0, 0);
        _instantiatedWaterfall.Color = _waterToAnimateList.Peek().Color;

        waterfallTransform.SetParent(transform.parent);
        waterfallTransform.eulerAngles = new Vector3(0, 0, 180);
        float newHeight = waterfallTransform.position.y - _secondFlaskAnimationController
                .GetComponent<Flask>().WaterInside.Peek().transform.position.y +
            _secondFlaskAnimationController._waterToAnimateList.Count - 1.2f;

        DOVirtual.Float(_instantiatedWaterfall.Height, newHeight, _baseTimeOfAnimation / 2,
                (height) => { _instantiatedWaterfall.Height = height; })
            .OnComplete(() => StartPouringAnimation?.Invoke());
    }

    private void EndWaterfallAnimation()
    {
        DOVirtual.Float(_instantiatedWaterfall.Height, 0, _baseTimeOfAnimation / 2, (height) =>
        {
            _instantiatedWaterfall.transform.position -= new Vector3(0, _instantiatedWaterfall.Height - height);

            _instantiatedWaterfall.Height = height;
        }).OnComplete(() => { Destroy(_instantiatedWaterfall.gameObject); });
    }

    private IEnumerator FlaskEmptyAnimation(int numberInFlask)
    {
        Transform flaskTransform = transform;
        float requiredOffset = _baseRequiredOffset + 2 * numberInFlask;
        float requiredAngle = Mathf.Atan(requiredOffset) * (int) _flaskSide;
        float currentAngle = transform.eulerAngles.z * Mathf.Deg2Rad;

        CalculateAnimationRotationSpeed(Mathf.Sin(requiredAngle) * Mathf.Cos(currentAngle) -
                                        Mathf.Cos(requiredAngle) * Mathf.Sin(currentAngle));

        while (Mathf.Cos(flaskTransform.rotation.eulerAngles.z * Mathf.Deg2Rad) > Mathf.Cos(requiredAngle))
        {
            flaskTransform.RotateAround(RotationPoint, Vector3.forward, _animationRotationSpeed * Time.deltaTime);
            yield return null;
        }

    }

    private void CalculateAnimationRotationSpeed(float deltaSinAngle)
    {
        float newAngleSpeed = Mathf.Asin(deltaSinAngle) * Mathf.Rad2Deg / _timeOfAnimation * 2;
        _animationRotationSpeed = 0;
        DOVirtual.Float(_animationRotationSpeed, newAngleSpeed, (1 - _speedReductionMultiplier) * _baseTimeOfAnimation,
            n => _animationRotationSpeed = n);
    }
    
    private void AimFlaskOnSecondFlask(Rect secondFlaskRect)
    {
        _animationFlaskPath = new Vector3[1];
        float shiftX = _flaskSide == FlaskSide.Left ? 0 : -0.6f;

        _animationFlaskPath[0] = _secondFlaskAnimationController.transform.localPosition +
                                 new Vector3(secondFlaskRect.width / 2 + shiftX, 1f);
        transform.DOLocalPath(_animationFlaskPath, _baseTimeOfAnimation / 2.0f, PathType.CatmullRom, PathMode.Ignore);
    }

    private void AimFlaskBack(Vector3 returnPoint)
    {
        _animationFlaskPath = new Vector3[1];
        _animationFlaskPath[0] = returnPoint;
        transform.DOLocalPath(_animationFlaskPath, _baseTimeOfAnimation / 2, PathType.CatmullRom, PathMode.Ignore,
            gizmoColor: Color.black).IsComplete();
    }

    private IEnumerator EmptyAnimation(WaterPart waterToAnimate)
    {
        while (!waterToAnimate.MeshEditor.IsShrinkedToZero)
        {
            waterToAnimate.MeshEditor.Shrink(_animationSpeed * Time.deltaTime);
            yield return null;
        }

        ObjectPool.Instance.SetPooledWater(waterToAnimate);
    }

    private IEnumerator PourAnimation(WaterPart waterToAnimate)
    {
        Transform waterPartTransform = waterToAnimate.transform;

        while (waterPartTransform.localScale.y < 1)
        {
            waterPartTransform.localScale += new Vector3(0, _animationSpeed * Time.deltaTime, 0);
            yield return null;
        }

        waterPartTransform.localScale = new Vector3(1, 1, 1);
    }
}