using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(Toggle))]
public class ToggleSpritesChanger : MonoBehaviour
{
    [SerializeField] private Sprite _spriteOn;
    [SerializeField] private Sprite _spriteOff;
    private Toggle _toggle;
    private Image _imageToChange;

    private void OnEnable()
    {
        _toggle = GetComponent<Toggle>();
        _imageToChange = GetComponent<Image>();
        _toggle.onValueChanged.AddListener(OnToggleSwitched);
    }

    private void OnToggleSwitched(bool state)
    {
        _imageToChange.sprite = state ? _spriteOn : _spriteOff;
    }
}