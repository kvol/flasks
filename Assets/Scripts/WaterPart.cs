using DG.Tweening;
using UnityEngine;

public class WaterPart : Water
{
    
    public WaterMeshEditor MeshEditor { get; private set; }


    protected override void Awake()
    {
        base.Awake();
        MeshEditor = GetComponent<WaterMeshEditor>();
        MeshEditor.enabled = false;
    }

    public void EnableWaterMeshEditorScript()
    {
        MeshEditor.enabled = true;
    }

    public void DisableWaterMeshEditorScript()
    {
        MeshEditor.enabled = false;
    }

    public int GetNumberRelativeToTopOfFlask()
    {
        return -1* (1 + (int) transform.localPosition.y);
    }

  
}