using System.Collections.Generic;
using UnityEngine;

public static class ProjectRectUtils
{
    public static Rect Encompass(List<Rect> rects)
    {
        if (rects.Count == 0)
            return Rect.zero;

        float xMin = rects[0].xMin,
            xMax = rects[0].xMax,
            yMin = rects[0].yMin,
            yMax = rects[0].yMax;


        for (int i = 1; i < rects.Count; i++)
        {
            xMin = Mathf.Min(xMin, rects[i].xMin);
            xMax = Mathf.Max(xMax, rects[i].xMax);
            yMin = Mathf.Min(yMin, rects[i].yMin);
            yMax = Mathf.Max(yMax, rects[i].yMax);
        }
        Rect rect = Rect.MinMaxRect(xMin, yMin, xMax, yMax);
        return rect;
    }
}