﻿using UnityEngine;

public class Water : MonoBehaviour,IMaterialChanger
{
    [SerializeField] private ColorsList _color;
    private static readonly int Color1 = Shader.PropertyToID("_Color");
    private Material _material;
    
    public ColorsList Color
    {
        get => _color;
        set
        {
            _color = value;
            _material.SetColor(Color1, value.GetColor());
        }
    }
    
    protected virtual void Awake()
    {
        _material = TryGetComponent(out MeshRenderer meshRenderer) ? meshRenderer.material : GetComponent<SpriteRenderer>().material;
        _material.SetColor(Color1, _color.GetColor());
    }

    public Material GetMaterial()
    {
        return  _material;
    }

    public void SetMaterial(Material material)
    {
        _material = material;
        GetComponent<MeshRenderer>().material = _material;
        Color = Color;
    }
    


}