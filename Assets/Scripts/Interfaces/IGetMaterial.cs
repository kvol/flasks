﻿using UnityEngine;


public interface IMaterialChanger
{
    Material GetMaterial();
    void SetMaterial(Material material);
}