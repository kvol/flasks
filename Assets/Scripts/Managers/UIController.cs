using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : Singleton<UIController>
{
    [SerializeField] private List<WindowBase> _windowBaseList;
    private WindowBase _currentOpenWindow;
    private bool _playerHasControl;


    public bool PlayerHasControl
    {
        get => _playerHasControl;
        set
        {
            if (value)
            {
                _currentOpenWindow?.SubscribeAllActions();
            }
            else
            {
                _currentOpenWindow?.UnsubscribeAllActions();
            }

            _playerHasControl = value;
        }
    }

    private void Start()
    {
        GameController.Instance.GameStateChanged += OnGameStateChanged;

        PlayerHasControl = true;
    }

    private void OnGameStateChanged(GameController.GameState state)
    {
        DisableUI();
        switch (state)
        {
            case GameController.GameState.Menu:
                ActivateWindow(WindowType.Menu);
                break;
            case GameController.GameState.LevelCreation:
                ActivateWindow(WindowType.Load);
                break;
            case GameController.GameState.Game:
                ActivateWindow(WindowType.Game);
                break;
            case GameController.GameState.RestartLevel:
                break;
            case GameController.GameState.LevelComplete:
                ActivateWindow(WindowType.Complete);
                break;
            case GameController.GameState.SettingsMenu:
                ActivateWindow(WindowType.Settings);
                ActivateWindow(WindowType.Game);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    private void ActivateWindow(WindowType windowType)
    {
        _currentOpenWindow = _windowBaseList[(int) windowType];
        _currentOpenWindow.Open();
    }

    private void DisableUI()
    {
        _currentOpenWindow?.Close();
        _currentOpenWindow = null;
    }

    private enum WindowType
    {
        Menu = 0,
        Game = 1,
        Load = 2,
        Complete = 3,
        Settings = 4
    }
}