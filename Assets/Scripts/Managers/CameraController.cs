using System;
using UnityEngine;

[RequireComponent(typeof(Camera), typeof(Transform))]
public class CameraController : MonoBehaviour
{
    [SerializeField] private Vector2 _screenOffset;
    private Transform _transform;
    private Camera _camera;

    private void Awake()
    {
        _transform = transform;
        _camera = GetComponent<Camera>();
        GameController.Instance.GameStateChanged += OnGameStateChanged;
    }

    private void OnGameStateChanged(GameController.GameState state)
    {
        switch (state)
        {
            case GameController.GameState.Menu:
                break;
            case GameController.GameState.LevelCreation:
                break;
            case GameController.GameState.Game:
                SetCameraOnFlasks();
                break;
            case GameController.GameState.RestartLevel:
                break;
            case GameController.GameState.LevelComplete:
                break;
            case GameController.GameState.SettingsMenu:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    private void SetCameraOnFlasks(){
    
        Rect flasksRect = LevelManager.Instance.FlasksRect;

        Vector3 position = flasksRect.center;
        position += new Vector3(0, 0, -10);
        _transform.position = position;

        float sizeHorizontalToVertical = (flasksRect.width / 2f + _screenOffset.x) / _camera.aspect;
        float sizeVertical = flasksRect.height / 2f + _screenOffset.y;
        _camera.orthographicSize =
            (sizeHorizontalToVertical > sizeVertical ? sizeHorizontalToVertical : sizeVertical);
    }
}