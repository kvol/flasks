﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

class StepsBackController : Singleton<StepsBackController>
{
    
    private const int MaxRollBacksCount = 20;
    private int _playerStepsBackCount = 10;
    [SerializeField] private int _defaultStepsBackCount = 10;
    private LinkedList<PreviousStep> _previousStep;
    public event Action<int> StepsBackCountChanged;

    private int PlayerStepsBackCount
    {
        set
        {
            _playerStepsBackCount = value; 
            StepsBackCountChanged?.Invoke(_playerStepsBackCount);
        }
    }

    private void Start()
    {
        _previousStep = new LinkedList<PreviousStep>();
        GameController.Instance.GameStateChanged += OnGameStateChanged;
    }
    
    private struct PreviousStep
    {
        public Flask flaskPouringFrom;
        public Flask flaskPouringTo;
        public int waterPartsCount;

        public PreviousStep(Flask flaskPouringFrom, Flask flaskPouringTo, int waterPartsCount)
        {
            this.flaskPouringFrom = flaskPouringFrom;
            this.flaskPouringTo = flaskPouringTo;
            this.waterPartsCount = waterPartsCount;
        }
    }


    public void AddPreviousStep(Flask flaskPouringFrom, Flask flaskPouringTo, int waterPartsCount)
    {
        PreviousStep previousStep = new PreviousStep(flaskPouringFrom, flaskPouringTo, waterPartsCount);
        if (_previousStep.Count >= MaxRollBacksCount)
        {
            _previousStep.RemoveLast();
        }

        _previousStep.AddFirst(previousStep);
    }

    private void OnGameStateChanged(GameController.GameState state)
    {
        switch (state)
        {
            case GameController.GameState.Menu:
                break;
            case GameController.GameState.LevelCreation:
                break;
            case GameController.GameState.Game:
                DOVirtual.DelayedCall(0.1f,() => PlayerStepsBackCount = _defaultStepsBackCount);
                break;
            case GameController.GameState.RestartLevel:
                break;
            case GameController.GameState.LevelComplete:
                PlayerStepsBackCount = _defaultStepsBackCount;
                _previousStep.Clear();
                break;
            case GameController.GameState.SettingsMenu:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    public void RollBack()
    {
        if (_previousStep.Count == 0 || _playerStepsBackCount <= 0)
        {
            return;
        }

        _playerStepsBackCount--;
        StepsBackCountChanged?.Invoke(_playerStepsBackCount);
        PreviousStep previousStep = _previousStep.First();
        _previousStep.RemoveFirst();
        GameController.Instance.MoveWaterParts(previousStep.flaskPouringFrom, previousStep.flaskPouringTo,
            previousStep.waterPartsCount);
    }
}