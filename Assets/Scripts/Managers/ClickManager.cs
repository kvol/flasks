using DG.Tweening;
using UnityEngine;

public class ClickManager : Singleton<ClickManager>
{
    private Flask _firstFlask;
    private Flask _secondFlask;
    [SerializeField] private float _riseCoefficient;
    [SerializeField] private float _animationDuration;


    public void SelectFlask(Flask flaskToSelect)
    {
        bool isFlaskOccupied =
            GameController.Instance.FlaskOccupationList.TryGetValue(flaskToSelect, out var animationType);

        if (flaskToSelect.IsComplete || DOTween.TweensByTarget(flaskToSelect.transform)?.Count > 0)
        {
            Debug.Log("Return anim");
            return;
        }

        if (_firstFlask == null && !isFlaskOccupied)
        {
            if (flaskToSelect.WaterInside.Count <= 0) return;

            MarkFlask(flaskToSelect);
        }
        else if (_firstFlask != null)
        {
            if (isFlaskOccupied && animationType != AnimationController.AnimationType.PourAnimation)
                return;
            if (flaskToSelect == _firstFlask)
            {
                UnmarkFlask(_firstFlask);
                _firstFlask = null;
                return;
            }

            UnmarkFlask(_firstFlask, true);

            _secondFlask = flaskToSelect;

            GameController.Instance.TryPour(_firstFlask, _secondFlask);

            _firstFlask = null;
            _secondFlask = null;
        }
    }

    private void MarkFlask(Flask flask)
    {
        Transform flaskTransform = flask.transform;

        Vector3 destination = flaskTransform.localPosition + Vector3.up * _riseCoefficient;

        _firstFlask = flask;
        flask.transform.DOLocalMove(destination, _animationDuration).SetEase(Ease.InOutCirc);
    }

    private void UnmarkFlask(Flask flask, bool isInstant = false)
    {
        Transform flaskTransform = flask.transform;

        Vector3 destination = flaskTransform.localPosition + Vector3.down * _riseCoefficient;


        if (isInstant)
        {
            if (DOTween.TweensByTarget(flaskTransform)?.Count > 0)
            {
                DOTween.TweensByTarget(flaskTransform)?[0].OnComplete(() => flaskTransform.localPosition = destination);
            }
            else
            {
                flaskTransform.localPosition = destination;
            }
            
        }
        else
        {
            flaskTransform.DOLocalMove(destination, _animationDuration).SetEase(Ease.InOutCirc);
        }
    }
}