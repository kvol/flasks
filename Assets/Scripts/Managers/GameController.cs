using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Singleton<GameController>
{
    public Dictionary<Flask, AnimationController.AnimationType> FlaskOccupationList { get; private set; }

    public event Action<GameState> GameStateChanged;

    private GameState _currentState;

    private int _completedFlasksCount;
    
    private GameState CurrentState
    {
        get => _currentState;
        set
        {
            _currentState = value;
            GameStateChanged?.Invoke(CurrentState);
        }
    }

    public enum GameState
    {
        Menu,
        LevelCreation,
        Game,
        RestartLevel,
        LevelComplete,
        SettingsMenu
    }

    private void Awake()
    {
        FlaskOccupationList = new Dictionary<Flask, AnimationController.AnimationType>();
    }

    private void Start()
    {
        GameMenu();
    }

    public void GameMenu()
    {
        CurrentState = GameState.Menu;
    }

    public void SettingsMenu()
    {
        CurrentState = GameState.SettingsMenu;
    }
    
    public void TryPour(Flask firstFlask, Flask secondFlask)
    {
        var waterToPour = firstFlask.WaterInside.Peek();
        var waterInSecondFlask = secondFlask.WaterInside.Count < 1 ? waterToPour : secondFlask.WaterInside.Peek();

        if (secondFlask.WaterInside.Count >= Flask.MAXWater || waterToPour.Color != waterInSecondFlask.Color) return;
        var waterToPourAmount = 0;

        foreach (var waterPart in firstFlask.WaterInside)
        {
            if (waterPart.Color == waterToPour.Color)
            {
                waterToPourAmount++;
            }
            else
            {
                break;
            }
        }

        if (Flask.MAXWater - secondFlask.WaterInside.Count < waterToPourAmount)
        {
            waterToPourAmount = Flask.MAXWater - secondFlask.WaterInside.Count;
        }

        UIController.Instance.PlayerHasControl = false;
        StepsBackController.Instance.AddPreviousStep(firstFlask, secondFlask, waterToPourAmount);

        for (var i = 0; i < waterToPourAmount; i++)
        {
            var newWater = ObjectPool.Instance.GetPooledWater();
            newWater.Color = waterToPour.Color;
            secondFlask.WaterInside.Push(newWater);
        }

        firstFlask.Empty(waterToPourAmount, secondFlask.AnimationController);
        firstFlask.FlaskAnimationEnded += EndFlaskAnimationHandler;
        FlaskOccupationList.Add(firstFlask, AnimationController.AnimationType.EmptyAnimation);

        secondFlask.Pour(waterToPourAmount, firstFlask.AnimationController);
        secondFlask.FlaskAnimationEnded += EndFlaskAnimationHandler;
        if (!FlaskOccupationList.ContainsKey(secondFlask))
        {
            FlaskOccupationList.Add(secondFlask, AnimationController.AnimationType.PourAnimation);
        }
    }


    private void EndFlaskAnimationHandler(Flask flask)
    {
        flask.FlaskAnimationEnded -= EndFlaskAnimationHandler;
        if (!FlaskOccupationList.Remove(flask))
        {
            return;
        }

        if (FlaskOccupationList.Count == 0)
        {
            UIController.Instance.PlayerHasControl = true;
        }
    }

    public void ChangeLevel()
    {
        LevelManager.Instance.LevelReady += OnLevelReady;
        CurrentState = GameState.LevelCreation;
    }

    private void OnLevelReady()
    {
        LevelManager.Instance.LevelReady -= OnLevelReady;
        SubscribeOnAllFlasksCompletion();
        CurrentState = GameState.Game;
    }

    private void SubscribeOnAllFlasksCompletion()
    {
        foreach (var flask in LevelManager.Instance.FlasksList)
        {
            flask.FlaskComplete += OnFlaskComplete;
        }

        _completedFlasksCount = LevelManager.Instance.FlasksList.Count;
    }

    private void OnFlaskComplete(Flask flask)
    {
        flask.FlaskComplete -= OnFlaskComplete;
        _completedFlasksCount--;
        CheckLevelCompletion();
    }

    private void CheckLevelCompletion()
    {
        if (_completedFlasksCount <= 2)
        {
            EndLevel();
        }
    }

    public void RestartLevel()
    {
        CurrentState = GameState.RestartLevel;
    }
    
    private void EndLevel()
    {
        CurrentState = GameState.LevelComplete;
    }

    
    public void MoveWaterParts(Flask flaskPouringTo, Flask flaskPouringFrom, int waterPartsCount)
    {
        for (int i = 0; i < waterPartsCount; i++)
        {
            WaterPart waterToMove = flaskPouringFrom.WaterInside.Pop();
            flaskPouringTo.SetWaterPosition(waterToMove);
            flaskPouringTo.SetWaterMaterial(waterToMove);
            flaskPouringTo.WaterInside.Push(waterToMove);
        }
    }
}