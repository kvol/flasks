using UnityEngine;

public class SaveManager : Singleton<SaveManager>
{
    [SerializeField]private int _levelNumber = 0;
   [SerializeField]private bool _soundEnabled = true;
    [SerializeField]private bool _vibrationEnabled = true;

    public int LevelNumber
    {
        get => _levelNumber;
        set => _levelNumber = value;
    }


    public bool SoundEnabled
    {
        get => _soundEnabled;
        set => _soundEnabled = value;
    }

    public bool VibrationEnabled
    {
        get => _vibrationEnabled;
        set => _vibrationEnabled = value;
    }
    
    private void Start()
    {
        LoadAllFields();
    }

    private void OnDestroy()
    {
        SaveAllFields();
    }

    private void SaveAllFields()
    {
        PlayerPrefs.SetInt(nameof(_levelNumber), _levelNumber);
        PlayerPrefs.SetString(nameof(_soundEnabled), _soundEnabled.ToString());
        PlayerPrefs.SetString(nameof(_vibrationEnabled), _vibrationEnabled.ToString());
        PlayerPrefs.Save();
    }

    private void LoadAllFields()
    {
        CheckSavedFields();
        _levelNumber = PlayerPrefs.GetInt(nameof(_levelNumber));
        _soundEnabled = bool.Parse(PlayerPrefs.GetString(nameof(_soundEnabled)));
        _vibrationEnabled = bool.Parse(PlayerPrefs.GetString(nameof(_vibrationEnabled)));
    }

    private void CheckSavedFields()
    {
        if (PlayerPrefs.HasKey(nameof(_levelNumber)) &&
            PlayerPrefs.HasKey(nameof(_soundEnabled)) && PlayerPrefs.HasKey(nameof(_vibrationEnabled)))
        {
            return;
        }

        SaveAllFields();
    }
}