using System;
using UnityEngine;

public class MaterialChanger : Singleton<MaterialChanger>
{
    [SerializeField] private Material _materialPrefabFlask;
    [SerializeField] private Material _materialPrefabWater;
    private static readonly int StencilRef = Shader.PropertyToID("_StencilRef");

    public void ChangeMaterial(IMaterialChanger materialChanger, MaterialType type)
    {
        switch (type)
        {
            case MaterialType.Flask:
                materialChanger.SetMaterial(Instantiate(_materialPrefabFlask));
                break;
            case MaterialType.Water:
                materialChanger.SetMaterial(Instantiate(_materialPrefabWater));
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }

    public void ChangeMaterialRefValue(IMaterialChanger materialChanger, int number)
    {
        materialChanger.GetMaterial().SetInt(StencilRef, number);
    }

    public enum MaterialType
    {
        Flask,
        Water
    }
}