using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public Action LevelReady;

    [SerializeField] private Flask _flaskPrefab;
    [SerializeField] private Transform _flasksRootTransform;
    [SerializeField] [Range(0.01f, 10)] private float _flaskOffset;
    private const int FlasksLines = 2;

    private List<Flask> _flasksList;

    public List<Flask> FlasksList => _flasksList;

    [SerializeField] private int _currentLevel;

    
    private int _flasksPerLine;

    public Rect FlasksRect
    {
        get
        {
            List<Rect> rects = new List<Rect>();
            foreach (var flask in _flasksList)
            {
                rects.Add(flask.FlaskRect);
            }

            return ProjectRectUtils.Encompass(rects);
        }
    }
    private int CurrentLevel
    {
        get => _currentLevel;
        set
        {
            _currentLevel = value;
            SaveManager.Instance.LevelNumber = _currentLevel;
        }
    }

    private void Start()
    {
        GameController.Instance.GameStateChanged += OnGameStateChanged;
        _flasksList = new List<Flask>();
        CurrentLevel = SaveManager.Instance.LevelNumber;
    }

    private void OnGameStateChanged(GameController.GameState state)
    {
        switch (state)
        {
            case GameController.GameState.Menu:
                RemovePreviousLevel();
                break;
            case GameController.GameState.LevelCreation:
                NextLevel();
                break;
            case GameController.GameState.Game:
                break;
            case GameController.GameState.RestartLevel:
                GameController.Instance.ChangeLevel();
                break;
            case GameController.GameState.LevelComplete:
                CurrentLevel++;
                break;
            case GameController.GameState.SettingsMenu:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(FlasksRect.center, FlasksRect.size);
    }

    private void NextLevel()
    {
        RemovePreviousLevel();
        StartCoroutine(CreateLevel(Resources.Load<LevelData>("Level " + CurrentLevel)));
    }

    private void RemovePreviousLevel()
    {
        for (var i = 0; i < _flasksRootTransform.childCount; i++)
        {
            var flask = _flasksRootTransform.GetChild(i).GetComponent<Flask>();
            flask.SetToPoolAllWaterInside();
            _flasksList.Remove(flask);
            Destroy(flask.gameObject);
        }

        _flasksRootTransform.DetachChildren();
        _flasksRootTransform.position += new Vector3(40, 0, 0);
    }

    private IEnumerator CreateLevel(LevelData levelToCreat)
    {
        if (levelToCreat == null)
        {
            yield return null;
            CurrentLevel = 0;
            GameController.Instance.GameMenu();
            yield break;
        }

        var flasksCount = levelToCreat.FlasksList.Count;
        var flasks = levelToCreat.FlasksList;

        CalculateFlasksPerLine(flasksCount);
        for (var i = 0; i < flasksCount; i++)
        {
            var newFlask = Instantiate(_flaskPrefab);
            var newFlaskTransform = newFlask.transform;
            newFlaskTransform.SetParent(_flasksRootTransform);
            SetFlaskPosition(newFlaskTransform, i, flasksCount);
            newFlask.Number = i + 2;
            MaterialChanger.Instance.ChangeMaterial(newFlask, MaterialChanger.MaterialType.Flask);
            MaterialChanger.Instance.ChangeMaterialRefValue(newFlask, newFlask.Number);

            _flasksList.Add(newFlask);
            foreach (var waterPartColor in flasks[i].WaterPartsColorList)
            {
                var newWaterPart = ObjectPool.Instance.GetPooledWater();
                var newWaterPartTransform = newWaterPart.transform;
                newWaterPartTransform.SetParent(newFlaskTransform);
                newWaterPartTransform.localScale = Vector3.one;
                MaterialChanger.Instance.ChangeMaterialRefValue(newWaterPart, newFlask.Number);

                newWaterPartTransform.localPosition = new Vector2(newFlask.FlaskRect.width / 2,
                    -(newFlask.FlaskRect.height - newFlask.WaterInside.Count));
                newWaterPart.Color = waterPartColor;
                newFlask.WaterInside.Push(newWaterPart);
            }
        }

        yield return null;
        LevelReady?.Invoke();
    }

    private void CalculateFlasksPerLine(int flasksNumber)
    {
        _flasksPerLine = flasksNumber / FlasksLines + (flasksNumber % FlasksLines == 0 ? 0 : 1);
    }

    //TODO Made Centering of flasks if there is not enough flasks in line
    private void SetFlaskPosition(Transform flaskTransform, int numberOfFlask, int flasksCount)
    {
        Vector2 offsetVector;
        Flask flask = flaskTransform.GetComponent<Flask>();

        var numberOfFlaskInLine = numberOfFlask % _flasksPerLine;
        var numberOfLine = numberOfFlask / _flasksPerLine;
        int flasksInLastLine = flasksCount - (FlasksLines - 1) * _flasksPerLine;

        if (numberOfLine == FlasksLines - 1 && flasksInLastLine != _flasksPerLine)
        {
            float lineLenght = FlasksRect.width;
            float newFlaskOffset = (lineLenght - flasksInLastLine * flask.FlaskRect.width) /
                                   (flasksInLastLine + 1);
            offsetVector = new Vector2(
                (newFlaskOffset + flask.FlaskRect.width) * numberOfFlaskInLine + newFlaskOffset,
                numberOfLine * (flask.FlaskRect.height + newFlaskOffset) * -1);
        }
        else
        {
            offsetVector = new Vector2(numberOfFlaskInLine * (flask.FlaskRect.width + _flaskOffset),
                numberOfLine * (flask.FlaskRect.height + _flaskOffset) * -1);
        }

        Vector3 localPosition = new Vector3(offsetVector.x, offsetVector.y);

        flaskTransform.localPosition = localPosition;
        flask.BasePosition = localPosition;
    }
}