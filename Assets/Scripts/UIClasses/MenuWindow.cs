﻿using UnityEngine;

public class MenuWindow : WindowBase
{
    [SerializeField]private SButton _startButton;
    
    public override void SubscribeAllActions()
    {
        _startButton.ButtonPressed += OnStartButtonPressed;
    }

    public override void UnsubscribeAllActions()
    {
        _startButton.ButtonPressed -= OnStartButtonPressed;
    }
    
    private void OnStartButtonPressed()
    {
        GameController.Instance.ChangeLevel();
    }
}