using System;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Toggle))]
public class SToggle : MonoBehaviour
{
    private Toggle _toggle;
    public event Action<bool> ToggleSwitched;
    
    private void Awake()
    {
        _toggle = GetComponent<Toggle>();
        _toggle.onValueChanged.AddListener(OnToggleSwitched);
    }

    private void OnToggleSwitched(bool state)
    {
        ToggleSwitched?.Invoke(state);
    }

    private void OnDestroy()
    {
        _toggle.onValueChanged.RemoveListener(OnToggleSwitched);
    }}
