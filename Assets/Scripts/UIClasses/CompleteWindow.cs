﻿using UnityEngine;

public class CompleteWindow : WindowBase
{
    [SerializeField] private SButton _nextButton;

    public override void SubscribeAllActions()
    {
        _nextButton.ButtonPressed += OnNextButtonPressed;
    }

    public override void UnsubscribeAllActions()
    {
        _nextButton.ButtonPressed -= OnNextButtonPressed;
    }

    private void OnNextButtonPressed()
    {
        GameController.Instance.ChangeLevel();
    }
}