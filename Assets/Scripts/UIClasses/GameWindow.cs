﻿using System;
using TMPro;
using UnityEngine;

public class GameWindow : WindowBase
{
    [SerializeField]private SButton _stepBackButton;
    [SerializeField]private SButton _menuButton;
    [SerializeField]private SButton _restartButton;
    [SerializeField]private SButton _settingsButton;
    [SerializeField]private TextMeshProUGUI _stepsBackRemainLabel;

    public override void SubscribeAllActions()
    {
        _stepBackButton.ButtonPressed += OnStepBackButtonPressed;
        _menuButton.ButtonPressed += OnMenuButtonPressed;
        _restartButton.ButtonPressed += OnRestartButtonPressed;
        _settingsButton.ButtonPressed += OnSettingsButtonPressed;
        StepsBackController.Instance.StepsBackCountChanged += OnStepsChanged;
    }


    public override void UnsubscribeAllActions()
    {
        _stepBackButton.ButtonPressed -= OnStepBackButtonPressed;
        _menuButton.ButtonPressed -= OnMenuButtonPressed;
        _restartButton.ButtonPressed -= OnRestartButtonPressed;
        _settingsButton.ButtonPressed -= OnSettingsButtonPressed;
        StepsBackController.Instance.StepsBackCountChanged -= OnStepsChanged;

    }

    private void OnSettingsButtonPressed()
    {
        GameController.Instance.SettingsMenu();
    }

    private void OnRestartButtonPressed()
    {   
        GameController.Instance.RestartLevel();
    }


    private void OnStepBackButtonPressed()
    {
        StepsBackController.Instance.RollBack();
    }
    
    private void OnMenuButtonPressed()
    {
        GameController.Instance.GameMenu();
    }
    
    private void OnStepsChanged(int newStepsCount)
    {
        _stepsBackRemainLabel.text = Convert.ToString(newStepsCount > 0 ? newStepsCount : 0);
    }
}