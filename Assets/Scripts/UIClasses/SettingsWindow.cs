﻿using UnityEngine;

public class SettingsWindow : WindowBase
{
    [SerializeField] private SToggle _musicSwitchButton;
    [SerializeField] private SToggle _vibrationSwitchButton;
    [SerializeField] private SButton _closeSettingsButton;
    public override void SubscribeAllActions()
    {
        _musicSwitchButton.ToggleSwitched += OnMusicSwitchButtonPressed;
        _vibrationSwitchButton.ToggleSwitched += OnVibrationSwitchButtonPressed;
        _closeSettingsButton.ButtonPressed += OnCloseSettingsButtonPressed;

    }

    public override void UnsubscribeAllActions()
    {
        _musicSwitchButton.ToggleSwitched -= OnMusicSwitchButtonPressed;
        _vibrationSwitchButton.ToggleSwitched -= OnVibrationSwitchButtonPressed;
        _closeSettingsButton.ButtonPressed -= OnCloseSettingsButtonPressed;
    }

    private void OnCloseSettingsButtonPressed()
    {
        Close();
    }

    private void OnVibrationSwitchButtonPressed(bool state)
    {
        
    }

    private void OnMusicSwitchButtonPressed(bool state)
    {
        
    }
}