using System;
using UnityEngine;


public enum ColorsList
{
    Yellow,
    Blue,
    Orange,
    Red,
    Green,
    Purple,
    Grey,
    Cian
}

public static class ColorsListExtensions
{
    public static Color GetColor(this ColorsList colorNum)
    {
        return colorNum switch
        {
            ColorsList.Yellow => Color.HSVToRGB(0.15f, 0.9f, 1f),
            ColorsList.Blue => Color.HSVToRGB(0.65f, 1f, 1f),
            ColorsList.Orange => Color.HSVToRGB(0.08f, 0.87f, 1f),
            ColorsList.Red => Color.HSVToRGB(0.99f, 0.91f, 1f),
            ColorsList.Green => Color.HSVToRGB(0.32f, 0.83f, 1f),
            ColorsList.Purple => Color.HSVToRGB(0.79f, 0.97f, 1f),
            ColorsList.Grey => Color.HSVToRGB(0.67f, 0.01f, 0.29f),
            ColorsList.Cian => Color.HSVToRGB(0.5f, 1f, 1f),
            _ => throw new ArgumentOutOfRangeException(nameof(colorNum), colorNum, null)
        };
    }
}