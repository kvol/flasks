Shader "Stencil/SpriteRead"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        [IntRange] _StencilRef ("Stencil Reference Value", Range(0,255)) = 0

    }
    SubShader
    {
        Tags
        {
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }


        Stencil
        {
            Ref [_StencilRef]
            Comp Equal
        }

        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            fixed4 _Color;


            v2f vert(appdata v)
            {
                v2f o;
                o.uv = v.uv;
                o.color = v.color;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                //Dissolve function

                fixed4 texColor = i.color * _Color;
                return texColor;
            }
            ENDCG
        }
    }
}