using UnityEngine;

public class WaterMeshEditor : MonoBehaviour
{
    private Mesh _mesh;
    private Transform _parentTransform;
    private Transform _transform;
    [SerializeField] private Vector3[] _defaultVertArray = null;
    [SerializeField] private Vector3[] _beginVertArray;
    private Vector3? _startPosition;

    [SerializeField] private Vector3[] _vertArray;
    [SerializeField] private float _shiftMultiplier;

    private float YOffset { get; set; }

    public bool IsShrinkedToZero => IsFirstFlask?(_beginVertArray[2].y - _beginVertArray[0].y+3) <= 0: (_beginVertArray[2].y - _beginVertArray[0].y) <= 0;

    private bool IsFirstFlask => (_transform.localPosition.y  + _shiftMultiplier * YOffset <= -4f);
    //mesh vertices write: left -> right, bottom -> top

    private void OnEnable()
    {
        _mesh = GetComponent<MeshFilter>().mesh;
        _vertArray = _mesh.vertices;
        _beginVertArray = new Vector3[4];
        _vertArray.CopyTo(_beginVertArray, 0);

        if (_defaultVertArray.Length == 0)
        {
            _defaultVertArray = new Vector3[4];
            _vertArray.CopyTo(_defaultVertArray, 0);
        }

        _transform = transform;
        _parentTransform = _transform.parent;
        _startPosition = null;
    }

    private void SetBeginVertArrayToDefault()
    {
        _defaultVertArray.CopyTo(_beginVertArray, 0);
    }

    private void Update()
    {
        EditMesh();
    }


    private void EditMesh()
    {
        _startPosition ??= _transform.localPosition;

        float flaskRotationZ = _parentTransform.rotation.eulerAngles.z;
        YOffset = Mathf.Abs(Mathf.Tan(flaskRotationZ * Mathf.Deg2Rad));

        for (int i = 0; i < _vertArray.Length; i++)
            _vertArray[i] = _beginVertArray[i];

        if (flaskRotationZ <= 180)
        {
            _vertArray[0] += new Vector3(0, YOffset, 0);
            _vertArray[2] += new Vector3(0, YOffset, 0);
        }
        else
        {
            _vertArray[1] += new Vector3(0, YOffset, 0);
            _vertArray[3] += new Vector3(0, YOffset, 0);
        }

        _transform.localPosition = _startPosition.Value - new Vector3(0, _shiftMultiplier * YOffset, 0);

        if (IsFirstFlask)
        {
            _vertArray[0] -= new Vector3(0, YOffset, 0);
            _vertArray[1] -= new Vector3(0, YOffset, 0);
        }

        _mesh.vertices = _vertArray;

        _mesh.RecalculateBounds();
    }

    private void OnDisable()
    {
        YOffset = 0;
        _transform.rotation = new Quaternion();
        _transform.localPosition = _startPosition.GetValueOrDefault();
        SetBeginVertArrayToDefault();
        _mesh.vertices = _beginVertArray;
    }

    public void Shrink(float deltaShrink)
    {
        _beginVertArray[2] -= new Vector3(0, deltaShrink);
        _beginVertArray[3] -= new Vector3(0, deltaShrink);

        if (!IsFirstFlask) return;
        _beginVertArray[2] -= new Vector3(0, 5f * deltaShrink, 0);
        _beginVertArray[3] -= new Vector3(0, 5f * deltaShrink, 0);
    }
}