using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : Singleton<ObjectPool>
{
    public List<WaterPart> pooledWater;
    [SerializeField]private WaterPart _waterToPoolPrefab;
    public int amountToPool;

    private void Start()
    {
        pooledWater = new List<WaterPart>();
        for (var i = 0; i < amountToPool; i++)
        {
            var tmp = Instantiate(_waterToPoolPrefab, transform, true);
            MaterialChanger.Instance.ChangeMaterial(tmp,MaterialChanger.MaterialType.Water);
            tmp.gameObject.SetActive(false);
            pooledWater.Add(tmp);
        }
    }

    public WaterPart GetPooledWater()
    {
        for (var i = 0; i < amountToPool; i++)
        {
            if (pooledWater[i].gameObject.activeInHierarchy) continue;
            pooledWater[i].transform.localScale = Vector3.zero;
            pooledWater[i].gameObject.SetActive(true);
            return pooledWater[i];
        }

        return null;
    }

    public void SetPooledWater(WaterPart waterToPool)
    {
        waterToPool.transform.SetParent(transform);
        waterToPool.gameObject.SetActive(false);
        waterToPool.DisableWaterMeshEditorScript();
    }
}